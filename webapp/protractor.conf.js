const crew = require('serenity-js/lib/stage_crew');

exports.config = {
    baseUrl: 'http://app:3000',
    seleniumAddress: 'http://firefox-container:4444/wd/hub',


    allScriptsTimeout: 110000,

    // Framework definition - tells Protractor to use Serenity/JS
    framework: 'custom',
    //frameworkPath:  require.resolve('protractor-cucumber-framework'),
    frameworkPath: require.resolve('serenity-js'),

    // Serenity with cucumber

    serenity: {
        dialect: 'cucumber',
        crew:    [
            crew.serenityBDDReporter(),
            crew.photographer()
        ]
    },


    specs: [ 'e2e-tests/features/add_menu_item.feature' ],

    cucumberOpts: {
          require:    [           // loads step definitions:
              'e2e-tests/features/**/add_menu_item.steps.ts' // - defined using JavaScript
          ],
          format: 'pretty',               // enable console output
          compiler: 'ts:ts-node/register'
    },


    capabilities: {
          browserName: 'firefox',

           'moz:firefoxOptions': {
              args: [ "--headless" ]
            }
    },

    restartBrowserBetweenTests: false
};