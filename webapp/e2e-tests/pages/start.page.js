var logger = require('../logger');


const StartPage = function() {

    const po = this;


    po.name = by.id('customerName');
    po.address = by.id('address');
    po.loginBtn = by.css('.btn-primary');

    po.doLogin = async function (name, address) {
        await browser.manage().deleteAllCookies();
        await browser.get(browser.baseUrl + '/index.html#/customer');

        await browser.element(po.name).sendKeys(name);
        await browser.element(po.address).sendKeys(address);
        await browser.element(po.loginBtn).click();

    }

};

module.exports = new StartPage();