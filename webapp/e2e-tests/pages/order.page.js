const format = require('util').format;

var logger = require('../logger');

const OrderPage = function() {

    const po = this;

    po.getOrderItemQty = async function (itemName) {

        let items = await browser.element.all(by.repeater('item in cart.items')).filter(function (row) {
            return row.evaluate("item.name").then(function (name) {
                return name === itemName;
            });
        });

        let qty = await items[0].evaluate('item.qty');

        return parseInt(qty);
    }
};

module.exports = new OrderPage();