
# Automation test practice how to

## Prerequisites

 - Docker
 - Docker-sync


## BDD Test Automation

1. Start guest environment

    ```bash
    $> docker-compose build
    $> docker-compose up
    ```
1. Connect to container 'foodme_app'

    ```bash
    $> docker ps -a
    $> docker exec -it CONTAINER_ID bash
    $> cd webapp
    ```
    
    As CONTAINER_ID use the identifier that maps to IMAGE foodme_app.

1. Execute tests

    ```bash
    $> npm run test:e2e
    ```






```bash
$> docker build -t node-docker .

```bash
$> docker run -p 49160:3000 -d node-docker
```

We can see all containers, also non running ones with:

```bash
$> docker ps -a
```

CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS                     PORTS               NAMES
8bea21baaa25        node-docker         "npm start"         3 minutes ago       Exited (1) 3 minutes ago                       silly_ptolemy

Here we see execution has aborted, maybe because our code. We can see logs using the container id:

```bash
$> docker logs CONTAINER_ID
```

If everything is OK we'll see port forwarding in the console:

```bash
$> docker ps -a

CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS                     NAMES
40a03af93265        node-docker         "npm start"         5 seconds ago       Up 4 seconds        0.0.0.0:49160->8080/tcp   hardcore_lumiere
```

Remove container:

```bash
$> docker rm CONTAINER_ID -f
```

Remove image:

```bash
$> docker rmi IMAGEID -f
```

If we want to prune everything (stopped containers, all dangling images, ...):

```bash
$ docker system prune -a
$ docker container prune
```



We start with:
```bash
$> docker-compose up --build
```

Recreate with:

```bash
$> docker-compose up -d --force-recreate --build
$> docker-compose up
```

See contents in container:

```bash
$> docker exec -it CONTAINER_ID bash
```



## Test monitoring of performance

```bash
$> while true; do echo -n "example:$((RANDOM % 100))|c" | nc -w 1 -u graphite 8125; done
```

1. Grafite. Go to: http://localhost:81/ (Grafite).
    - User: root:root
    - http://localhost:81/render?from=-10mins&until=now&target=stats.example

1. Grafana

    - Go to: http://localhost (Grafana), and user admin:admin.
    - Visit http://localhost/datasources/new. Configure:

         * Name: 'upc-foodme'
         * Type: Graphite
         * Url: http://localhost:81

